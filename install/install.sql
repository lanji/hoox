CREATE TABLE IF NOT EXISTS `config` (
	`site_id` int(11) NOT NULL AUTO_INCREMENT,
	`site_theme` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
	`site_header` text COLLATE utf8_unicode_ci NOT NULL,  
	`site_footer` text COLLATE utf8_unicode_ci NOT NULL,
	`site_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
	`site_analytics` text COLLATE utf8_unicode_ci NOT NULL,
	PRIMARY KEY (`site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

CREATE TABLE IF NOT EXISTS `pages` (
	`page_id` int(11) NOT NULL AUTO_INCREMENT,
	`page_filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
	`page_parent` int(11) NOT NULL,
	`page_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
	`page_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
	`page_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
	`page_content` text COLLATE utf8_unicode_ci NOT NULL,
	`page_created` int(11) NOT NULL,
	`page_modified` int(11) NOT NULL,
	`page_published` int(11) NOT NULL,
	`page_deleted` int(11) NOT NULL,
	`page_homepage` int(11) NOT NULL,
	`page_navitem` int(11) NOT NULL,
	`page_order` int(11) NOT NULL,
	PRIMARY KEY (`page_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=38 ;

CREATE TABLE IF NOT EXISTS `sessions` (
	`id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
	`userid` int(11) NOT NULL,
	`created` int(11) NOT NULL,
	`lastmod` int(11) NOT NULL,
	`lastip` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `users` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`username` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
	`email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
	`password` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
	`created` int(11) NOT NULL,
	`lastlogin` int(11) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;