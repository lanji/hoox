<?php
require("class.install.php");
$installer = new HooxInstaller();
?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>HOOX CMS INSTALLATION</title>
    <style>
        body{font-family:Arial,sans-serif; padding:25px; font-size:9pt;}
        div.install{width:500px; background:#EEE; margin:0 auto; border:1px solid #333; padding:10px;}
        div.urgent{width:500px; background:#FDA; margin:0 auto; border:5px solid #F90; padding:10px; color:#F90; font-size:12pt; font-weight:bold; margin-bottom:20px;}
        h1{color:#F90; text-align:center;}
    </style>
</head>
<body>
	<div class="urgent">
    <?php
		$installer->printMessages();
    ?>
	</div>
<div class="install">
    <h1>HOOX Installation*</h1>
    <ol>
        <li>Create your MySQL database/user.</li>
		<li>Make includes directory writeable so installer can save configuration.</li>
        <li>Fill out and submit this form.</li>
        <li>Once installation is confirmed, delete the "install" folder on your server.</li>
		<li>Make your includes directory non-writeable!</li>
    </ol>
    <em>*Requires server with PHP 5+, a MySQL database and FTP access to your server to delete install files after the site is set up.</em>
    <form action="<?php print $_SERVER["PHP_SELF"];?>" method="post">
    <table>
        <tr><td>DB Host:</td><td><input type="text" name="host" value="localhost" /></td></tr>
        <tr><td>DB Name:</td><td><input type="text" name="database" /></td></tr>
        <tr><td>DB User:</td><td><input type="text" name="user" /></td></tr>
        <tr><td>DB Password:</td><td><input type="password" name="password" /></td></tr>
		<tr><td>Admin Username:</td><td><input type="text" name="admin_user" /></td></tr>
		<tr><td>Admin Email:</td><td><input type="text" name="admin_email" /></td></tr>
		<tr><td>Admin Password:</td><td><input type="password" name="admin_pass" /></td></tr>
		<tr><td>Site Title:</td><td><input type="text" name="site_name" /></td></tr>
        <tr><td colspan="2"><input type="hidden" name="install" value="true" /><input type="submit" value="Install Hoox" /></td></tr>
    </table>
    </form>
</div>
</body>
</html>