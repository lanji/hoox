<?php
class HooxInstaller {
	public $messages = array();
	
	public function __construct() {
		if(!isset($_POST['install'])) {
			return;
		}
		$this->addMessage(($this->stringIsValid($_POST['host'])) ? '' : 'Invalid database host.');
		$this->addMessage(($this->stringIsValid($_POST['database'])) ? '' : 'Invalid database name.');
		$this->addMessage(($this->stringIsValid($_POST['user'])) ? '' : 'Invalid database user.');
		$this->addMessage(($this->stringIsValid($_POST['password'])) ? '' : 'Invalid database password.');
		$this->addMessage(($this->stringIsValid($_POST['admin_user'])) ? '' : 'Invalid admin name.');
		$this->addMessage(($this->stringIsValid($_POST['admin_email'])) ? '' : 'Invalid admin email.');
		$this->addMessage(($this->stringIsValid($_POST['admin_pass'])) ? '' : 'Invalid admin password.');
		$this->addMessage(($this->stringIsValid($_POST['site_name'])) ? '' : 'No site name specified.');
		if(count($this->messages) == 0) {
			$this->install();
		}
	}
	
	public function install() {
		if($this->createDatabase()) {
			if($this->saveSettings()) {
				$this->addMessage("Install completed successfully! Don't forget:");
				$this->addMessage("Delete install directory.");
				$this->addMessage("Make includes NOT writeable");
				return true;
			}
		}
	}
	
	public function saveSettings() {
		$fh = fopen("../includes/settings.php", "w");
		if($fh) {
			$settings = '<?php $host = "' . $_POST['host'] . '"; $user = "' . $_POST['user'] . '"; $pass = "' . $_POST['password'] . '"; $db = "' . $_POST['database'] . '"; ?>';
			if(!fwrite($fh, $settings)) {
				$this->addMessage("Failed to write to settings file.");
				return false;
			}
			fclose($fh);
			$this->addMessage("Successfully wrote settings.");
			return true;
		}
		$this->addMessage("Failed to open settings file. Invalid permission?");
		return false;
	}
	
	public function createDatabase() {
		$sqlFile = "install.sql";
		
		if($conn = mysql_connect($_POST['host'], $_POST['user'], $_POST['password'])) {
			if(!mysql_select_db($_POST['database'])) {
				$this->addMessage("Failed to select database: " . mysql_error());
				return false;
			}
			$fh = fopen($sqlFile, "r");
			if($fh) {
				$sql = fread($fh, filesize($sqlFile));
				$sql = explode(";", $sql);
				fclose($fh);
				for($i = 0; $i < count($sql); $i++) {
					if($sql[$i] == '') {
						continue;
					}
					if(!mysql_query($sql[$i], $conn)) {
						$this->addMessage("Failed to execute install query: " . mysql_error());
						return false;
					}
				}
				$this->addMessage("Tables created successfully");
				return $this->insertSiteData($conn);
			}
			$this->addMessage("Failed to open sql file.");
			return false;
		}
		$this->addMessage("Failed to connect to database. Incorrect settings?");
		return false;
	}
	
	public function insertSiteData($conn) {
		$adminName = mysql_real_escape_string($_POST['admin_user']);
		$adminEmail = mysql_real_escape_string($_POST['admin_email']);
		$adminPassword = md5($_POST['admin_pass']);
		$siteName = mysql_real_escape_string($_POST['site_name']);
		
		// save site config
		$configSql = "INSERT INTO config VALUES (1, 'default', '', '', '$siteName', '')";
		if(!mysql_query($configSql, $conn)) {
			$this->addMessage("Failed to save site config: " . mysql_error());
			return false;
		}
		
		// save admin user
		$adminSql = "INSERT INTO users VALUES(1, '$adminName', '$adminEmail', '$adminPassword', " . time() . ", " . time() . ")";
		if(!mysql_query($adminSql, $conn)) {
			$this->addMessage("Failed to save admin user: " . mysql_error());
			return false;
		}
		
		// create first page
		$pageSql = "INSERT INTO pages VALUES(1, 'home', 0, 'Home', '', '', '<h1>Your first page</h1>', " . time() . ", " . time() . ", 1, 0, 1, 0, 0)";
		if(!mysql_query($pageSql, $conn)) {
			$this->addMessage("Failed to save starter page: " . mysql_error());
			return false;
		}
		
		$this->addMessage("Starter data saved successfully!");
		return true;
	}
	
	public function addMessage($str) {
		if($str == '') {
			return;
		}
		$this->messages[] = $str;
	}
	
	public function printMessages() {
		if(count($this->messages) == 0) {
			return;
		}
		for($i = 0; $i < count($this->messages); $i++) {
			print "<p>" .$this->messages[$i] . "</p>";
		}
	}
	
	public function stringIsValid($str) {
		if(isset($str) && $str != null && $str != '') {
			return true;
		}
		return false;
	}
}
// EOF