<?php
/*=======================================
    PHP HTML CLASS
    
    Provides Link and HTML encoding
    helper functions
    
    Justin Johnson, 2010
=======================================*/
class HTML
{
    
    function HTML()
    {
    }
  
    function Safe($str)
    {
        return htmlspecialchars($str,ENT_QUOTES);
    }
    
    function UnSafe($str)
    {
        return htmlspecialchars_decode($str,ENT_QUOTES);
    }
}