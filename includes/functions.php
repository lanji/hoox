<?php
// create master navigation
function NavigationList($parentId = 0, $activeBranch = FALSE)
{
    global $mysql, $html, $page;
    $items = $mysql->PagesByParentId($parentId);
    if($items == FALSE)
        return;
    $output;
    for($i = 0; $i < count($items); $i++)
    {
        // handle styles
        $classAppend = ($i == 0) ? " first" : "";
        $classAppend .= ($i == count($items) - 1) ? " last" : "";

        // determine if this branch is active
        $activeNode = FALSE;
        if($items[$i]['page_id'] == $page['id'])
        {
            $activeNode = TRUE;
            $activeBranch = TRUE;
            $classAppend .= " active";
        }

        // create url and output line, recurse
        $url = Url($items[$i]['page_filename'],$items[$i]['page_id']);
        $output .= "<li class='nav_item".$classAppend."'><a href='".$url."' ".(($activeNode) ? "class=' active'" : '').">".$items[$i]['page_title']."</a>";
        $output .= NavigationList($items[$i]['page_id'], $activeNode);
        $output .= "</li>";
    }
    $output = "<ul ".(($activeBranch) ? "class=' active'" : '').">".$output."</ul>";
    return $output;
}

function EmailSignup()
{
    $output = "<form>

    </form>";
    return $output;
}

// create a breadcrumb of chained categories
function BreadCrumb($pageId)
{
    global $mysql, $html;
    if($pageId === 0 || $pageId == '')
        return "<a href='/' title='Home'>Home</a>";
    else
    {
        $pgData = $mysql->PageById($pageId);
        $crumb = " > <a href='".Url($pgData['page_filename'], $pageId)."' title='".$html->Safe($pgData['page_title'])."'>".$pgData['page_title']."</a>";
        $parent = $mysql->ParentData($pageId);
        return BreadCrumb($parent['parent_id']) . $crumb;
    }
}

// load and populate a theme
function RenderPage($page, $config)
{
    global $html;
    ob_start();
    include 'themes/'.$page['theme'].'/template.html';
    $output = ob_get_contents();
    ob_end_clean();

    $logo = "<a href='/' title='".$html->Safe($config['site_title'])."'><img src='/themes/".$page['theme']."/logo.png' /></a>";
    $message = ($page['message'] == '') ? '' : "<div class='urgent'>".$page['message']."</div>";
    $title = ($config['site_title'] != '') ? $config['site_title'].$page['title'] : $page['title'];

    $output = str_replace('%%PAGE_TITLE%%',$title,$output);
    $output = str_replace('%%PAGE_KEYWORDS%%',$html->Safe($page['keywords']),$output);
    $output = str_replace('%%PAGE_DESCRIPTION%%',$html->Safe($page['description']),$output);
    $output = str_replace('%%PAGE_THEME%%','/themes/'.$html->Safe($page['theme']).'/screen.css',$output);
    $output = str_replace('%%PAGE_BREADCRUMB%%', BreadCrumb($page['id']),$output);
    $output = str_replace('%%PAGE_CONTENT%%',$page['content'],$output);
    $output = str_replace('%%PAGE_NAVIGATION%%',$page['navigation'],$output);
    $output = str_replace('%%PAGE_HEADER%%',$page['header'],$output);
    $output = str_replace('%%PAGE_FOOTER%%',$page['footer'],$output);
    $output = str_replace('%%PAGE_ANALYTICS%%',$page['analytics'],$output);
    $output = str_replace('%%PAGE_LOGO%%',$logo,$output);
    $output = str_replace('%%PAGE_MESSAGE%%',$message,$output);

    return $output;
}

// retrieve cached page
function RenderCache($f)
{
    $filename = 'cache/'.md5($f).'.cache';
    if(file_exists($filename))
    {
        ob_start();
        include($filename);
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }
    return FALSE;
}

// create a cached page
function CachePage($f, $data)
{
    $filename = 'cache/'.md5($f).'.cache';
    if($fh = fopen($filename, "w"))
    {
        fwrite($fh, $data);
        fclose($fh);
        return TRUE;
    }
    return FALSE;
}

// generate a page url
function Url($pageFilename, $pageId)
{
    global $mysql;
    if($pageId === 0 || $pageId == '')
        return "/";
    $url = $pageFilename."-p".$pageId."/";
    $parent = $mysql->ParentData($pageId);
    return Url($parent['parent_filename'], $parent['parent_id']) . $url;
}
?>