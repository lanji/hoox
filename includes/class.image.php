<?php
/*=======================================
    PHP Image CLASS
    
    Handles image uploads, image lists,
    tinyMCE functionality
    
    Justin Johnson, 2010
=======================================*/
class Image
{
    var $path;
    var $src_path;
    var $max_size = 1024;
    var $msg_log = array();
    var $thumb_size = array();
    
    function Image($path = 'uploads', $thumbSize = array(125,125))
    {
        $this->path = $_SERVER['DOCUMENT_ROOT']."/".$path."/";
        $this->src_path = "/".$path."/";
        $this->thumb_size = $thumbSize;
    }
    
    function SaveImage($image)
    {
        $errors = 0;
        if($image)
        {
            if($image['error'] != UPLOAD_ERR_OK && $image['size'] <= 0)
            {
                $this->LogMsg("ERROR: File was not uploaded");
                $errors++;
            }
            else
            {
                $fname = $this->Filename($image['name']);
                $newName = $fname[0]."_".time().".".$fname[1];
                $data = getimagesize($image['tmp_name']);
                
                if($fname[1] != 'jpg' && $fname[1] != 'gif' && $fname[1] != 'png')
                {
                    $this->LogMsg("ERROR: Unsupported filetype.");
                    $errors++;
                }
                if(filesize($image['tmp_name']) >= $this->max_size * 1024)
                {
                    $this->LogMsg("ERROR: File is too large.");
                    $errors++;
                    return FALSE;
                }
                if(!is_array($data) || $data[0] <= 0 || $data[1] <= 0)
                {
                    $this->LogMsg("ERROR: Bad image size data. Invalid image?");
                    $errors++;
                    
                }
                if(copy($image['tmp_name'],$this->path.$newName))
                {
                    $this->LogMsg("Saved file as: ".$this->path.$newName."<br />\n");
                }
                else
                {
                    $this->LogMsg("ERROR: Failed to copy file. Bad server permissions?");
                    $errors++;
                }
                
                if($this->Thumbnail($image,$fname[1],$data,$newName))
                {
                    $errors++;
                }
            }
        }
        else
        {
            $this->LogMsg("ERROR: Image doesn't exist.");
            $errors++;
        }
        
        if($errors == 0)
            return TRUE;
        else
            return FALSE;
        
    }
    
    function Filename($str)
    {
        $i = strpos($str, ".");
        if(!$i)
        {
            return FALSE;
        }
        else
        {
            $l = strlen($str) - $i;
            $ext = strtolower(substr($str, $i + 1, $l));
            $file = substr($str, 0, $i);
            return array($file, $ext);
        }
    }
    
    function GetScaleSize($x, $y, $xMax, $yMax)
    {
        if($x > $y)
        {
            $newX = $xMax;
            $newY = floor($y * ($newX / $x));
        }
        else
        {
            $newY = $yMax;
            $newX = floor($x * ($newY / $y));
        }
        $size[0] = $newX;
        $size[1] = $newY;
        return $size;
    }
    
    function SetTransparency($imgDest, $imgSrc)
    {
        $index = imagecolortransparent($imgSrc);
        $color = array('red' => 255, 'green' => 255, 'blue' => 255);
        if($index >= 0)
            $color = imagecolorsforindex($imgSrc, $index);
        
        $index = imagecolorallocate($imgDest, $color['red'], $color['green'], $color['blue']);
        imagefill($imgDest, 0, 0, $index);
        imagecolortransparent($imgDest, $index);
    }
    
    function Thumbnail($image, $type, $size, $filename)
    {
        $scaleSize = $this->GetScaleSize($size[0],$size[1],$this->thumb_size[0],$this->thumb_size[1]);
        $errors = 0;
        $filename = "thumb_".$filename;
        switch($type)
        {
            case 'jpg' :
                $orig = imagecreatefromjpeg($image['tmp_name']);
                $thumb = imagecreatetruecolor($scaleSize[0],$scaleSize[1]);
                imagecopyresampled($thumb, $orig, 0, 0, 0, 0, $scaleSize[0], $scaleSize[1], $size[0], $size[1]);
                if(!imagejpeg($thumb, $this->path.$filename, 65))
                {
                    $this->LogMsg("ERROR: Failed to create jpg thumbnail.");
                    $errors++;
                }
            break;
            case 'png' :
                $orig = imagecreatefrompng($image['tmp_name']);
                $thumb = imagecreatetruecolor($scaleSize[0],$scaleSize[1]);
                $this->SetTransparency($thumb, $orig);
                imagecopyresampled($thumb, $orig, 0, 0, 0, 0, $scaleSize[0], $scaleSize[1], $size[0], $size[1]);
                if(!imagepng($thumb, $this->path.$filename, 3))
                {
                    $this->LogMsg("ERROR: Failed to create png thumbnail.");
                    $errors++;
                }
            break;
            case 'gif' :
                $orig = imagecreatefromgif($image['tmp_name']);
                $thumb = imagecreatetruecolor($scaleSize[0],$scaleSize[1]);
                $this->SetTransparency($thumb, $orig);
                imagecopyresampled($thumb, $orig, 0, 0, 0, 0, $scaleSize[0], $scaleSize[1], $size[0], $size[1]);
                if(!imagegif($thumb, $this->path.$filename))
                {
                    $this->LogMsg("ERROR: Failed to create gif thumbnail.");
                    $errors++;
                }
            break;
        }
        
        imagedestroy($thumb);
        
        if($errors == 0)
        {
            $this->LogMsg("Successfully created thumbnail: ".$this->path.$filename);
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    
    function ThumbList($path = null)
    {
        if($path == null)
            $path = $this->src_path;
        $imgArray = array();
        if($dh = opendir($this->path))
        {
            while(($file = readdir($dh)) !== FALSE)
            {
                if($file == ".." || $file == ".")
                {
                    continue;
                }
                $f = $this->Filename($file);
                if(($f[1] == 'jpg' || $f[1] == 'gif' || $f[1] == 'png') && (strpos($f[0],"thumb_") !== FALSE))
                {
                    $imgArray[] = $path.$file;
                }
            }
            sort($imgArray);
            
            return $imgArray;
        }
        else
        {
            $this->LogMsg("ERROR: Failed to open directory.");
            return FALSE;
        }
        
    }
    
    function LogMsg($str)
    {
        $this->msg_log[] = $str;
    }
    
    function DumpLog()
    {
        $output;
        foreach($this->msg_log as $msg)
            $output .= $msg."<br />\n";
        return $output;
    }
    
}