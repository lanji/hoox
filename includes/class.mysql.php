<?php
/*=======================================
    PHP MYSQL CLASS
    
    Provides easy database connection and
    some quick query functionality.
    
    Justin Johnson, 2010
=======================================*/
class Mysql
{
    var $connection;
    var $log;
    var $queryCount;
    
    // CONSTRUCTOR
    function Mysql($host, $user, $password, $database)
    {
        $this->connection = mysql_connect($host,$user,$password) or $this->Error(TRUE,"Uh oh, bad connection to database: ".mysql_error());
        $this->queryCount = 0;
        if (!mysql_select_db($database))
        {
            $this->Error(true,"Uh oh, we can't find $database: " . mysql_error());
        }
    }
    
    //-----------------------------------
    // SITE FUNCTIONS
    //-----------------------------------
    
    function SiteTheme()
    {
	$result = $this->ReadQuery("SELECT site_theme FROM config LIMIT 1");
	if($result !== FALSE)
	    return $result[0]['site_theme'];
	else
	    return FALSE;
    }
    
    function SiteConfig()
    {
	$result = $this->ReadQuery("SELECT * FROM config LIMIT 1");
	if($result !== FALSE)
	    return $result[0];
	else
	    return FALSE;
    }
    
    function UpdateConfig($site, $theme, $header, $footer, $analytics)
    {
	$sql = "UPDATE config SET
	    site_analytics = '".$this->Safe($analytics)."',
	    site_theme = '".$this->Safe($theme)."',
	    site_title = '".$this->Safe($site)."',
	    site_header = '".$this->Safe($header)."',
	    site_footer = '".$this->Safe($footer)."'";
	$this->WriteQuery($sql);
    }
    
    function ClearHomepage()
    {
	$this->WriteQuery("UPDATE pages SET page_homepage = 0 WHERE page_homepage = 1");
    }
    
    function GetHomepage()
    {
	$result = $this->ReadQuery("SELECT * FROM pages WHERE page_homepage = 1 LIMIT 1");
	if($result !== FALSE)
	    return $result[0];
	else
	    return FALSE;
    }
    
    function ActivePages()
    {
	return $this->ReadQuery("SELECT * FROM pages WHERE page_deleted = 0 AND page_published = 1 AND page_homepage = 0");
    }
    
    function PageById($id)
    {
        $result = $this->ReadQuery("SELECT * FROM pages WHERE page_id = ".$this->Safe($id));
	if($result !== FALSE)
	    return $result[0];
	else
	    return FALSE;
    }
    
    function PagesByParentId($id, $navOnly = TRUE)
    {
	$sql = "SELECT * FROM pages WHERE page_parent = ".$this->Safe($id)." AND page_deleted = 0 ";
	if($navOnly)
	    $sql .= "AND page_homepage = 0 AND page_navitem = 1 AND page_published = 1 ";
	$sql .= "ORDER BY page_order ASC";
	return $this->ReadQuery($sql);
    }
    
    function ParentData($pageId)
    {
	$result = $this->ReadQuery("SELECT p2.page_filename as parent_filename, p2.page_id as parent_id FROM pages p1 LEFT JOIN pages p2 ON p1.page_parent = p2.page_id WHERE p1.page_id = ".$this->Safe($pageId));
	if($result === FALSE)
	    return array("parent_id" => 0, "parent_filename" => "root");
	else
	    return $result[0];
    }
    
    //-----------------------------------
    // AUTH FUNCTIONS
    //-----------------------------------
    function ValidateUser($user, $pass)
    {
	$valid = $this->ReadQuery("SELECT id FROM users WHERE username = '".$this->Safe($user)."' AND password = '".md5($pass)."'");
	if($valid !== FALSE)
	    return $valid[0]['id'];
	else
	    return FALSE;
    }
    
    function SessionKey()
    {
	$id = $this->ReadQuery("SELECT UUID() as sessionkey");
	return $id[0]['sessionkey'];
    }
    
    function SessionData($sessionId)
    {
	$sessionData = $this->ReadQuery("SELECT userid, lastmod FROM sessions WHERE id = '".$this->Safe($sessionId)."' LIMIT 1");
	return ($sessionData !== FALSE) ? $sessionData[0] : $sessionData;
    }
    
    function SessionCreate($key, $user)
    {
	$sql = "INSERT INTO sessions(id, userid, created, lastmod, lastip)
	VALUES('".$key."', ".$user.", ".time().", ".time().", '".$_SERVER['REMOTE_ADDR']."')";
	$this->WriteQuery($sql);
    }
    
    function SessionRenew($sessionId)
    {
	$this->WriteQuery("UPDATE sessions SET lastmod = ".time().", lastip = '".$_SERVER['REMOTE_ADDR']."' WHERE id = '".$sessionId."'");
    }
    
    function SessionDestroy($sessionId)
    {
	$this->WriteQuery("DELETE FROM sessions WHERE id = '".$this->Safe($sessionId)."'");
    }
    
    //-----------------------------------
    // QUERY WRAPPER FUNCTIONS
    //-----------------------------------
    function ReadQuery($sql)
    {
	$this->queryCount++;
	$result;
	$result = mysql_query($sql,$this->connection) or $this->Error(TRUE,'Uh oh, big read query fail on: '.$sql.' and the error gobbledegook: '.mysql_error());
	if(mysql_num_rows($result))
	{
            $output;
            while($row = mysql_fetch_assoc($result))
            {
		$output[] = $row;
            }
	    return $output;
	}
	else
	{
	    $this->Error(FALSE,"We couldn't find any results for: ".$sql);
	    return FALSE;
	}
    }
    
    function WriteQuery($sql)
    {
	$this->queryCount++;
	$result = mysql_query($sql,$this->connection) or $this->Error(TRUE,'Uh oh, big read query fail on: '.$sql.' and the error gobbledegook: '.mysql_error());
    }
    
    
    //-----------------------------------
    // HELPER FUNCTIONS
    //-----------------------------------
    function Error($critical, $str)
    {
        if($critical === TRUE)
        {
            $this->log[] = $str;
            die($this->PrintLog());
        }
        else
        {
            $this->log[] = $str;
        }
    }
    
    function PrintLog()
    {
        $output = '<div class="fail">';
        for($i = 0; $i < count($this->log); $i++)
        {
            $output .= $this->log[$i].'<br />';
        }
        $output .= '</div>';
        return $output;
    }
    
    function Safe($str)
    {
        return mysql_real_escape_string($str);
    }
    
    function FileName($str)
    {
	$filename = strtolower($str);
	$filename = preg_replace("/[^a-z0-9\- _]/","",$filename);
	return preg_replace("/[^a-z0-9]/","-",$filename);
    }
}