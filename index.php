<?php
/*============================================
 
    HOOX CMS
    SIMPLE PHP ENGINE
    Justin Johnson 2010
    
============================================*/
if(file_exists('includes/settings.php'))
   require('includes/settings.php');
else
    header("Location: /install/");

require('includes/config.php');
require('includes/functions.php');

//-------------------------------
// RENDER
//-------------------------------
$cachedPage = RenderCache($_SERVER['REQUEST_URI']);

if($cachedPage === FALSE)
{
    $theme = $mysql->SiteTheme();
    
    if(isset($_GET['p']))
    {
        $pgData = $mysql->PageById($_GET['p']);
    }
    else
    {
        $pgData = $mysql->GetHomepage();
    }
    
    if($pgData !== FALSE)
    {
        $page['id'] = $_GET['p'];
        $page['title'] = $pgData['page_title'];
        $page['keywords'] = $pgData['page_keywords'];
        $page['description'] = $pgData['page_description'];
        $page['parent'] = $pgData['page_parent'];
        $page['content'] = $pgData['page_content'];
        $page['theme'] = ($theme) ? $theme : 'default';
    }
    else
    {
        $page['id'] = null;
        $page['title'] = "404: The requested page was not found.";
        $page['keywords'] = '';
        $page['description'] = '';
        $page['parent'] = 0;
        $page['content'] = "<h1>ERROR: 404</h1> Sorry, this page was not found on our server!";
        $page['theme'] = 'default';
    }
    
    $page['message'] = '';
    $page['analytics'] = $config['site_analytics'];
    $page['header'] = $config['site_header'];
    $page['footer'] = $config['site_footer'];
    $page['navigation'] = NavigationList();
    
    if(file_exists('install/index.php'))
    {
        $page['message'] .= "**WARNING** INSTALL DIRECTORY STILL EXISTS. DELETE IT NOW AND CLEAR CACHE IN ADMIN AREA! **WARNING**";
    }
    
    $newPage = RenderPage($page, $config);
    CachePage($_SERVER['REQUEST_URI'],$newPage);
    print $newPage;
}
else
{
    print $cachedPage;
}
?>
