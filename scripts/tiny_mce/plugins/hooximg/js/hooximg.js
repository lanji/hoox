tinyMCEPopup.requireLangPack();

var HooximgDialog = {
	init : function()
	{
		/*
		var f = document.forms[0];
		// Get the selected contents as text and place it in the input
		f.someval.value = tinyMCEPopup.editor.selection.getContent({format : 'text'});
		f.somearg.value = tinyMCEPopup.getWindowArg('some_custom_arg');
		*/
	},
	
	insert : function(file)
	{
		var ed = tinyMCEPopup.editor, dom = ed.dom;
		tinyMCEPopup.execCommand('mceInsertContent', false, "<img src='"+file+"' />");
		tinyMCEPopup.close();
	}
};

tinyMCEPopup.onInit.add(HooximgDialog.init, HooximgDialog);
