<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>{#hooximg_dlg.title}</title>
	<script type="text/javascript" src="../../tiny_mce_popup.js"></script>
	<script type="text/javascript" src="js/hooximg.js"></script>
</head>
<body>

<h1>Upload an Image:</h1>
<form enctype='multipart/form-data' action='<?php print $_SERVER['PHP_SELF'];?>' method='post'>
    Image:<input type='file' name='image' /><br />
    <input type='submit' name='upload' value='Upload' />
</form>

<?php
if(isset($_POST['upload']))
{
	require($_SERVER['DOCUMENT_ROOT']."/includes/class.image.php");
	$img = new Image();
	$save = $img->SaveImage($_FILES['image']);
	if($save)
	    print "Save success: ".$img->DumpLog();
	else
	    print $img->DumpLog();
}
?>

</body>
</html>
