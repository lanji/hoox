<?php
require($_SERVER['DOCUMENT_ROOT']."/includes/class.image.php");
$img = new Image();
$msg;

// is user trying to delete images?
if(isset($_GET['del']) && (strpos($_GET['del'], ".gif") || strpos($_GET['del'], ".jpg")))
{
	// we need to make sure user is authorized
	if(file_exists($_SERVER['DOCUMENT_ROOT'].'/includes/settings.php'))
	require($_SERVER['DOCUMENT_ROOT'].'/includes/settings.php');
	else
	    header("Location: /install/");
	require($_SERVER['DOCUMENT_ROOT'].'/includes/config.php');
	require($_SERVER['DOCUMENT_ROOT'].'/admin/includes/functions.php');
	require($_SERVER['DOCUMENT_ROOT'].'/admin/includes/authcheck.php');
	
	// delete image
	if($validUser == TRUE)
	{
		if(unlink($_SERVER['DOCUMENT_ROOT'] . $_GET['del']))
		{
			$msg = $_GET['del'] . " deleted successfully.";
		}
		else
		{
			$msg = "Failed to delete file.";
		}
	}
	else
	{
		$msg = "You do not have permission to delete this file.";
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>{#hooximg_dlg.title}</title>
	<script type="text/javascript" src="../../tiny_mce_popup.js"></script>
	<script type="text/javascript" src="js/hooximg.js"></script>
	<style>
		ul{padding:0; margin:0;}
		li{list-style:none; padding:0 0 20px 0; margin:0;}
		a{text-decoration:none;}
	</style>
</head>
<body>
	<?php if($msg != '') { print $msg; } ?>

<h1>Select an Image to Insert:</h1><?php

print "<ul>";
foreach($img->ThumbList() as $thumb)
{
	print "
	<li>
		<a href='javascript:HooximgDialog.insert(\"".str_replace("thumb_","",$thumb)."\");'>
		<img src='".$thumb."' /><br />
		(".str_replace("/uploads/thumb_","",$thumb).")
		</a> (<a onclick='javascript:return confirm(\"Deleting an image is not undoable. Pages that feature this image will have broken links. Are you sure you want to delete?\");' href='".$_SERVER['PHP_SELF']."?del=".$thumb."'>X</a>)
	</li>";
}
print "</ul>";

?>
</body>
</html>
