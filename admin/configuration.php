<?php
if(file_exists('../includes/settings.php'))
   require('../includes/settings.php');
else
    header("Location: /install/");
require('../includes/config.php');
require('includes/functions.php');
require('includes/authcheck.php');
//------------------------------
// PRE RENDER
//------------------------------

if(isset($_POST['saveconfig']))
{
    if(isset($_POST['theme']) && $_POST['theme'] != '')
    {
        $mysql->UpdateConfig($_POST['site'], $_POST['theme'], $html->Unsafe($_POST['header']), $_POST['footer'],$html->UnSafe($_POST['analytics']));
        $msg .= ClearCache();
        $msg .= "Site configuration updated.";

		// reload config
		$config = $mysql->SiteConfig();
    }
    else
    {
        $msg .= "Theme field missing. This is required.";
    }
}

//------------------------------
// RENDER
//------------------------------
    include('includes/gui.header.php');
?>
<h1>Site Configuration Settings</h1>
<form action='<?php print $_SERVER['PHP_SELF']; ?>' method='post'>
<table class='form'>
    <tr><td>Site Title:</td><td><input type='text' name='site' value='<?php print (is_array($config)) ? $config['site_title'] : "" ; ?>' /></td></tr>
    <tr><td>Site Theme:</td><td><?php print ThemeDropdown('theme', $config['site_theme']); ?></td></tr>
    <tr><td>Analytics Code:</td><td></td></tr>
    <tr><td colspan='2'><textarea name='analytics' class='small'><?php print (is_array($config)) ? $html->Safe($config['site_analytics']) : "" ; ?></textarea></td></tr>
    <tr><td>Header Text:</td><td></td></tr>
    <tr><td colspan='2'><textarea name='header' class='richtext_editor small'><?php print (is_array($config)) ? $html->Safe($config['site_header']) : "" ; ?></textarea></td></tr>
    <tr><td>Footer Text:</td><td></td></tr>
    <tr><td colspan='2'><textarea name='footer' class='richtext_editor small'><?php print (is_array($config)) ? $config['site_footer'] : "" ; ?></textarea></td></tr>
    <tr><td colspan='2'><input type='submit' value='SAVE' class='submit' /></td></tr>
</table>
<input type='hidden' name='saveconfig' value='true' />
</form>
<?php
    include('includes/gui.footer.php');
?>