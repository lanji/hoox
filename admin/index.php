<?php
if(file_exists('../includes/settings.php'))
   require('../includes/settings.php');
else
    header("Location: /install/");
require('../includes/config.php');
require('includes/functions.php');
require('includes/authcheck.php');
//------------------------------
// PRE RENDER
//------------------------------
if(isset($_GET['cache']))
{
    $msg = ClearCache();
}

//------------------------------
// RENDER
//------------------------------
    include('includes/gui.header.php');
?>
Welcome to the HOOX CMS Admin. This is where you manage your site's content,
change site configuration settings and perform maintenance tasks. Below is a list
of administrative tasks with a brief explanation of each one.

<h2>Clear Cache</h2>
HOOX CMS aggressively caches non-dynamic pages to improve the speed of the site. The cache for a
given page should automatically be cleared whenever that page is changed or updated.
However, things don't always go exactly as planned so HOOX gives you the ability to clear
all files in the cache. This may cause a brief performance dip as pages are re-cached.
Do this if page changes don't seem to be taking effect.<br />
<a href='/admin/?cache=clear' title='Clear Cache'>Clear Cache Now</a>

<h2>Site Config</h2>
This is where you go to set up your site for the first time or make sitewide changes.<br />
<a href='/admin/configuration.php' title='Configure Site'>Site Config</a>

<h2>Pages</h2>
This is the main area where you create and edit the important content for your site.
Pages can be parents of sub pages (like categories) and there can be infinite levels of nested subpages.
This is not recommended for usability but the system supports it.<br />
<br />
Note that final page may not look like what you see in the page editor. The page editor uses some basic
styles so you can tell different elements apart. The saved page will fit the carefully-planned design of
your site as long as you do not use custom colors or fonts.
<a href='/admin/pages.php' title='Create Pages'>Pages</a>

<?php
    include('includes/gui.footer.php');
?>