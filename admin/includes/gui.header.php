<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>HOOX CMS Admin</title>
    <link type='text/css' rel='stylesheet' media='all' href='admin.css' />
    <script type='text/javascript' src='/scripts/jquery.js'></script>
    <script type='text/javascript' src='/scripts/tiny_mce/jquery.tinymce.js'></script>
    <script type='text/javascript' src='/admin/includes/rte.js'></script>
</head>
<body>
    <div class='wrapper'>
		<div class='header'>
			<a href="/admin" title="Admin Home"><img src="/admin/images/hoox-logo.png" alt="Hoox Logo" /></a>
        </div>
        <div class='content_main'>
            <?php
                if(isset($msg) && $msg != '')
				{
                    print "<div class='urgent'>".$msg."</div>";
				}

				if(isset($validUser) && $validUser)
				{
					print "<div class='urgent'>This site is using database \"$db\" on \"$host\".</div>";
				}
            ?>

			<div class='content_right'>