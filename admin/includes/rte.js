$(document).ready(function()
{
    
    $(".richtext_editor").tinymce(
    {
        script_url : "../scripts/tiny_mce/tiny_mce.js",
        theme : "advanced",
        convert_urls : false,
        plugins : "table,searchreplace,paste,advimage,hooximg",
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,removeformat,styleselect,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "undo,redo,|,cut,copy,paste,pastetext,|,search,replace,|,justifyleft,justifycenter,justifyright,justifyfull,|,forecolor,backcolor,|,code",
        theme_advanced_buttons3 : "bullist,numlist,outdent,indent,blockquote,|,link,unlink,image,anchor,cleanup,|,tablecontrols,|,hooximg,hooxupload",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_blockformats : "div,p,h1,h2,h3,h4,h5,h6",
        content_css : "cms.css",
        style_formats : [
          {title: 'Urgent', inline:'div', classes:'urgent'}
        ]
    });
    
    $(".edit_toggle").toggle(
        function()
        {
            $(".richtext_editor").tinymce().hide();
        },
        function ()
        {
            $(".richtext_editor").tinymce().show();
        }
    );
    
});