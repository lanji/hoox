<?php
// create master navigation
function NavigationList($parentId = 0)
{
    global $mysql, $html;
    $pages = $mysql->PagesByParentId($parentId, FALSE);
    if($pages == FALSE)
        return;
    $output = "<ul>";
    for($i = 0; $i < count($pages); $i++)
    {
        $classAppend = ($i == 0) ? " first" : "";
        $classAppend .= ($i == count($pages) - 1) ? " last" : "";
        $url = "/admin/pages.php?edit=".$pages[$i]['page_id'];
        $output .= "<li class='nav_item".$classAppend."'><a href='".$url."'>".$pages[$i]['page_title']."</a>";
        $output .= NavigationList($pages[$i]['page_id']);
        $output .= "</li>";
    }
    $output .= "</ul>";
    return $output;
}

function ParentDropdown($name = 'parent', $default = 0)
{
    $output = "<select name='".$name."'>\n";
    $output .= "<option value='0' ".(($default === 0) ? 'selected' : '')." >ROOT</option>\n";
    $output .= ParentOptions($default);
    $output .= "</select>\n";
    return $output;
}

function ParentOptions($default, $id = 0, $indent = 0)
{
    global $mysql, $html;
    $output;
    $pages = $mysql->PagesByParentId($id);
    if($pages !== FALSE)
    {
        for($i = 0; $i < count($pages); $i++)
        {
            $output .= "<option value='".$pages[$i]['page_id']."' ".(($default == $pages[$i]['page_id']) ? 'selected' : '')." >";
            for($j = 0; $j < $indent; $j++)
            {
                $output .= "--";
            }
            $output .= $html->Safe($pages[$i]['page_title'])."</option>\n";
            $output .= ParentOptions($default, $pages[$i]['page_id'], $indent + 1);
        }
    }
    return $output;
}

function ThemeDropdown($name = 'theme', $default = 'default')
{
    $output = "<select name='".$name."'>";
    $fileArray;
    if($dh = opendir("../themes/"))
    {
        while(($file = readdir($dh)) !== FALSE)
        {
            if($file == ".." || $file == ".")
            {
                continue;
            }
            $fileArray[] = $file;
        }
    }
    
    sort($fileArray);
    
    foreach($fileArray as $f)
        $output .= "<option value='".$f."' ".(($default == $f) ? 'SELECTED' : '').">".$f."</option>";
    
    $output .= "</select>";
    return $output;
}

function ClearCache($page = "ALL")
{
    if($page == "ALL")
    {
        foreach(glob("../cache/*.cache") as $f)
        {
            $msg .= "Clearing Cache Page: ".$f."<br />\n";
            unlink($f);
        }
        return $msg;
    }
    else
    {
        // clear a single page
    }
}

function ExpireCookie($name)
{
    Cookie($name, null, time() - 3600);
}

function Cookie($name, $value, $time)
{
    setcookie($name, $value, $time, "/", $_SERVER['SERVER_NAME']);
}
?>