<?php
//===================================
// CHECK TO MAKE SURE CURRENT USER
// IS AUTHORIZED.
//===================================

$validUser = false;

if(isset($_COOKIE['session_id']))
{
    $session = $mysql->SessionData($_COOKIE['session_id']);
    if((time() - $session['lastmod']) < (30 * 60))
    {
        // session has not expired, renew everything
        Cookie('session_id',$_COOKIE['session_id'],time() + 30 * 60);
        $mysql->SessionRenew($_COOKIE['session_id']);
        $validUser = true;
    }
    else
    {
        // session expired, destroy and redirect
        ExpireCookie('session_id');
        $mysql->SessionDestroy($_COOKIE['session_id']);
        header("Location:/admin/login.php?msg=2");
    }
}
else
{
    // no cookie, redirect to login
    if($_SERVER['PHP_SELF'] !== '/admin/login.php')
        header("Location:/admin/login.php?msg=1");
}
?>