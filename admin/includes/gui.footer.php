            </div>
            <div class='content_left'>
                    <h3>Admin Options</h3>
					<ul>
						<?php
						if($validUser)
						{
							?>
							<li><a href='/admin/'>Admin Home</a></li>
							<li><a href='/admin/pages.php'>New Page</a></li>
							<li><a href='/admin/configuration.php'>Site Config</a></li>
							<li><a href='/admin/?cache=clear'>Clear Cache</a></li>
							<li><a href='/'>View Site</a></li>
							<?php
						}
						?>
						<li><a href='/admin/login.php?logout=true' title='Log out'>Logout</a></li>
					</ul>

					<h3>Site Pages</h3>
					<div class="navigation">
					<?php if($validUser) { print NavigationList(); } ?>
					</div>
            </div>
            <div class='clear'></div>
        </div>
    </div>
    <div class='footer'>
        HOOX CMS is built and maintained by Justin Johnson. Copyright 2011, All Rights Reserved.<br />
    </div>
</body>
</html>