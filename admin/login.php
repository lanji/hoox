<?php
if(file_exists('../includes/settings.php'))
   require('../includes/settings.php');
else
    header("Location: /install/");
require('../includes/config.php');
require('includes/functions.php');
//------------------------------
// PRE RENDER
//------------------------------
if(isset($_GET['msg']))
{
    switch($_GET['msg'])
    {
        case 1:
            $msg .= 'You must log in before using admin tools.<br />';
        break;
        case 2:
            $msg .= 'Your session is no longer valid. Please log in again to administer your site.<br />';
        break;
        case 3:
            $msg .= 'Login failed. Incorrect username or password.<br />';
        break;
        case 4:
            $msg .= 'Login successful. Your session will be
            cleared one hour after your last activity as admin.
            To ensure your site security, please log out when
            done to expire your session immediately.';
            $validUser = TRUE;
        break;
    }
}

if(isset($_POST['user']) && isset($_POST['pass']))
{
    $userId = $mysql->ValidateUser($_POST['user'], $_POST['pass']);
    if($userId !== FALSE)
    {
        $sessionKey = $mysql->SessionKey();
        Cookie('session_id', $sessionKey, time() + (60 * 60));
        $mysql->SessionCreate($sessionKey,$userId);
        header("Location:/admin/login.php?msg=4");
    }
    else
    {
        header("Location:/admin/login.php?msg=3");
    }
}

if(isset($_GET['logout']))
{
    $mysql->SessionDestroy($_COOKIE['session_id']);
    ExpireCookie('session_id');
    header("Location:/");
}



//------------------------------
// RENDER
//-----------------------------
include('includes/gui.header.php');
?>
<form action='<?php print $_SERVER['PHP_SELF']; ?>' method='post'>
<table class="form">
	<tr>
		<td>Username:</td><td><input type='text' name='user' /></td>
	</tr>
	<tr>
		<td>Password:</td><td><input type='password' name='pass' /></td>
	</tr>
	<tr>
		<td>&nbsp;</td><td><input type='submit' value='LOGIN' class="submit" /></td>
	</tr>
</table>
</form>

<?php
    include('includes/gui.footer.php');
?>