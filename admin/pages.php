<?php
if(file_exists('../includes/settings.php'))
   require('../includes/settings.php');
else
    header("Location: /install/");
require('../includes/config.php');
require('includes/functions.php');
require('includes/authcheck.php');
//------------------------------
// PRE RENDER
//------------------------------
if(isset($_POST['savepage']))
{
    if($_POST['homepage'] == 'yes')
       $mysql->ClearHomepage();
    $sql = "INSERT INTO pages(
    page_filename,
    page_parent,
    page_title,
    page_keywords,
    page_description,
    page_content,
    page_created,
    page_modified,
    page_published,
    page_deleted,
    page_homepage,
    page_navitem,
    page_order
    ) VALUES(
    '".$mysql->Safe($_POST['filename'])."',
    ".$mysql->Safe($_POST['parent']).",
    '".$mysql->Safe($_POST['title'])."',
    '".$mysql->Safe($_POST['keywords'])."',
    '".$mysql->Safe($_POST['description'])."',
    '".$mysql->Safe($_POST['content'])."',
    ".time().",
    ".time().",
    ".(($_POST['published'] == 'yes') ? 1 : 0).",
    0,
    ".(($_POST['homepage'] == 'yes') ? 1 : 0).",
    ".(($_POST['nav'] == 'yes') ? 1 : 0).",
    ".((isset($_POST['order']) && $_POST['order'] != '') ? $mysql->Safe($_POST['order']) : 0).")";
    $mysql->WriteQuery($sql);
    $msg .= ClearCache();
    $msg .= "Page saved!";
}

if(isset($_POST['editpage']))
{
    if($_POST['homepage'] == 'yes')
       $mysql->ClearHomepage();
    $sql = "UPDATE pages SET
    page_filename = '".$mysql->Safe($mysql->Filename($_POST['filename']))."',
    page_parent = ".$mysql->Safe($_POST['parent']).",
    page_title = '".$mysql->Safe($_POST['title'])."',
    page_keywords = '".$mysql->Safe($_POST['keywords'])."',
    page_description = '".$mysql->Safe($_POST['description'])."',
    page_content = '".$mysql->Safe($_POST['content'])."',
    page_modified = ".time().",
    page_homepage = ".(($_POST['homepage'] == 'yes') ? 1 : 0).",
    page_navitem = ".(($_POST['nav'] == 'yes') ? 1 : 0).",
    page_published = ".(($_POST['published'] == 'yes') ? 1 : 0).",
    page_order = ".((isset($_POST['order']) && $_POST['order'] != '') ? $mysql->Safe($_POST['order']) : 0)."
    WHERE page_id = ".$mysql->Safe($_POST['editpage']);
    $mysql->WriteQuery($sql);
    $msg .= ClearCache();
    $msg .= "Page Updated.";
}

if(isset($_GET['delete']))
{
    $mysql->WriteQuery("UPDATE pages SET page_deleted = 1 WHERE page_id = ".$mysql->Safe($_GET['delete']));
    $msg .= ClearCache();
    $msg .= "Page deleted!";
}

if(isset($_GET['edit']))
{
    $page = $mysql->PageById($_GET['edit']);
    if($page === FALSE)
    {
        $msg .= "Failed to pull page data.";
    }
}

//------------------------------
// RENDER
//------------------------------
    include('includes/gui.header.php');
?>

<h1>Add/Modify Page</h1>
    <form action='<?php print $_SERVER['PHP_SELF']; ?>' method='post'>
    <table class='form'>
        <tr><td>Title:</td><td><input type='text' name='title' value='<?php print (is_array($page)) ? $html->Safe($page['page_title']) : "" ; ?>' /></td></tr>
        <tr><td>Filename:</td><td><input type='text' name='filename' value='<?php print (is_array($page)) ? $html->Safe($page['page_filename']) : "" ; ?>' /></td></tr>
        <tr><td>Description:</td><td><input type='text' name='description' value='<?php print (is_array($page)) ? $html->Safe($page['page_description']) : "" ; ?>' /></td></tr>
        <tr><td>Keywords:</td><td><input type='text' name='keywords' value='<?php print (is_array($page)) ? $html->Safe($page['page_keywords']) : "" ; ?>' /></td></tr>
        <tr><td>Parent:</td><td><?php print ParentDropdown('parent',$page['page_parent']); ?></td></tr>
        <tr><td>Show In Nav:</td><td><input name='nav' value='yes' type='checkbox' <?php print ((is_array($page)) ? (($page['page_navitem'] == 1) ? "checked" : "" ) : "checked" ); ?> /></td></tr>
        <tr><td>Nav Order:</td><td><input name='order' value='<?php print (is_array($page)) ? $page['page_order'] : "0" ; ?>' /> </td></tr>
        <tr><td>Published?:</td><td><input name='published' value='yes' type='checkbox' <?php print ((is_array($page)) ? (($page['page_published'] == 1) ? "checked" : "" ) : "checked" ); ?> /></td></tr>
        <tr><td>Set as Home (will override other homepage):</td><td><input name='homepage' value='yes' type='checkbox' <?php print ((is_array($page)) ? (($page['page_homepage'] == 1) ? "checked" : "" ) : "" ); ?> /></td></tr>
        <tr><td colspan="2">Content:</td></tr>
        <tr><td colspan='2'><textarea class='richtext_editor' name='content'><?php print (is_array($page)) ? $page['page_content'] : "" ; ?></textarea></td></tr>
        <tr><td colspan='2'><input type='submit' value='SAVE' class="submit" /></td></tr>
    </table>
    <?php
        if(isset($_GET['edit']))
            print "<input type='hidden' name='editpage' value='".$_GET['edit']."' />";
        else
            print "<input type='hidden' name='savepage' value='true' />";
    ?>

    </form>
<?php
    include('includes/gui.footer.php');
?>