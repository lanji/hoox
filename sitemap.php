<?php
/*============================================
 
    SITEMAP GENERATOR
    Justin Johnson 2010
    
============================================*/
if(file_exists('includes/settings.php'))
   require('includes/settings.php');
else
    header("Location: /install/");

require('includes/config.php');
require('includes/functions.php');

$lastmodDefault = date("Y-m-d",time() - rand(0,864000));

function XMLEntry($loc, $priority = ".5", $lastmod = null, $changefreq = "weekly")
{
    global $lastmodDefault;
    $lastmod = ($lastmod == null) ? $lastmodDefault : date("Y-m-d", $lastmod);
    $output = "
    <url>
        <loc>".htmlspecialchars($loc,ENT_QUOTES)."</loc>
        <lastmod>".$lastmod."</lastmod>
        <changefreq>".$changefreq."</changefreq>
        <priority>".$priority."</priority>
    </url>";
    return $output;
}

$output;
$p = $mysql->ActivePages();
foreach($p as $page)
{
    $url = "http://".$_SERVER['HTTP_HOST'].Url($page['page_filename'], $page['page_id']);
    $output .= XMLEntry($url,".5",$page['page_modified']);
}

print
    '<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'.
    $output
    ."\n</urlset>";
?>