<?php
// Pull RSS feed if >1 day old or doesn't exist
if(!file_exists("feed.xml") || filemtime("feed.xml") < (time() - (24 * 60 * 60)))
{
    $feedUrl = "http://www.sierratradingpost.com/rss/";
    $ch = curl_init($feedUrl);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);
    $rss = curl_exec($ch);
    $fh = fopen("feed.xml", "w");
    fwrite($fh,$rss);
    fclose($fh);
    print "<!-- Fresh Feed Loaded -->\n";
}

// open and parse rss XML
$feed = file_get_contents("feed.xml");
$xml = simplexml_load_string($feed);
$items;
foreach($xml->channel->item as $item)
{
    $items[] = $item;
}

// Set number of displayed items and keycode
$numItems = (isset($_GET['items']) && ($_GET['items'] < 20)) ? $_GET['items'] : 4;
$keycode = (isset($_GET['kc'])) ? $_GET['kc'] : 'FSITE10';
$chosenProducts = array();
$count = 0;
$totalCount = 0;

// Render
print "<div class='adspace_label'>New Items! <a href='#' id='adspace_more_link'>(See More)</a></div>";
while($count < $numItems)
{
    $totalCount++;
    if($totalCount > 50)
    {
        print "<div class='product_link'>Error pulling products.</div>";
        break;
    }
    
    // choose random index, check for uniqueness and image existence
    $imgCode = null;
    $index = rand(0, count($items) - 1);
    if(preg_match("/ProductImages\/([^.]+)\.tif/", $items[$index]->description, $matches) != FALSE && array_search($index, $chosenProducts) === FALSE)
    {
        $imgCode = $matches[1];
        $chosenProducts[] = $index;
    }
    else
    {
        continue;
    }
    
    $href = preg_replace("/\?kc=.*/", "", $items[$index]->link)."?kc=".$keycode;
    print "<div class='product_link'><a href='".$href."' title='".htmlspecialchars($items[$index]->title,ENT_QUOTES)."'>";
    if($imgCode != null)
        print "<img src='http://i.stpost.com/erez4/erez?src=ProductImages/".$imgCode.".tif&tmp=Thumbnail&redirect=0' alt='".htmlspecialchars($items[$index]->title,ENT_QUOTES)."' /><br />";
    else
        print "<img src='/themes/stpjp/noimage.jpg' /><br />";
    print (strlen($items[$index]->title) > 25)? substr($items[$index]->title,0,22)."..." : $items[$index]->title;
    print "</a></div>\n";
    $count++;
}
print "<div style='clear:both;'></div>";


?>