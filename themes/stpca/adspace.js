$(document).ready(function()
{
    PopulateAdspace();
});

function PopulateAdspace()
{
    // get data
    $.get("/themes/stpca/new_item_display.php?items=4&utm_source=STPCA&utm_medium=International&utm_campaign=new+items&kc=INCA2", function(data)
    {
        $(".adspace").html(data);
        $(".adspace").show("fast");

        // attach click event
        $("a#adspace_more_link").click(function(event)
        {
            event.preventDefault();
            PopulateAdspace();
        });
    });


}

